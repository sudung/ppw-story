from django.shortcuts import render, redirect
from .models import *
from .forms import Activity_Form

def index(request):
    print('sini')
    return render(request, 'lab_5/index.html',)

def contact(request):
    return render(request, 'lab_5/contact.html',)

def project(request):
    return render(request, 'lab_5/project.html',)

def all_schedule(request):
    activities = Activity.objects.all()
    
    response = {'activities' : activities}
    return render(request, 'lab_5/all_schedule.html', response)

def delete_all(request):
    Activity.objects.all().delete()
    Category.objects.all().delete()
    
    return redirect('lab_5:all_schedule')

def activity_form(request):
    if request.method == 'POST' :
        form = Activity_Form(request.POST)
        if form.is_valid() :
            activity = Activity()
            category = Category(category = form.cleaned_data['category'])
            category.save()

            activity.activity_name = form.cleaned_data['activity_name']
            activity.time = form.cleaned_data['time']
            activity.location = form.cleaned_data['location']
            activity.category = category
            activity.save()

        return redirect('lab_5:all_schedule')
    else:
        form = Activity_Form()
        response = {'form' : form}
        return render(request, 'lab_5/form.html', response)