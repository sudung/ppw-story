from django.db import models
from django.utils import timezone
from datetime import datetime

class Category(models.Model) :
    HOBBY = 'Hobbies'
    OUTDOOR = 'Outdoor'
    INDOOR = 'Indoor'
    STUDY = 'Study'
    WORK = 'Work'
    MISC = 'Miscellaneous'
    
    CATEGORY_CHOICES = (
        (HOBBY, 'Hobbies'),
        (OUTDOOR, 'Outdoor'),
        (INDOOR, 'Indoor'),
        (STUDY, 'Study'),
        (WORK, 'Work'),
        (MISC, 'Miscellaneous'),
    )

    category = models.CharField(
        max_length = 2,
        choices = CATEGORY_CHOICES,
        default = OUTDOOR,
    )
    
class Activity(models.Model) :
    activity_name = models.CharField(max_length = 32)
    time = models.DateTimeField(default = timezone.now)
    location =  models.CharField(max_length = 40) 
    category = models.ForeignKey(Category, on_delete = models.CASCADE)