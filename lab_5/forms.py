from django import forms
from .models import Category

class Activity_Form(forms.Form):
    
    activity_name = forms.CharField(label='Name',
                    max_length = 27, widget = forms.TextInput(
                        attrs = {
                            'class':'form-control',
                            'placeholder':'Activity Name',
                            'required': True,
                        }
                    ))

    time = forms.DateTimeField(label='Date & Time',
                    widget = forms.DateTimeInput(
                        attrs = {
                            'class':'form-control',
                            'placeholder':'mm/dd/YY HH:MM  (Please use correct formatting and values)',
                            'required': True,
                        }
                    ))

    location = forms.CharField(label='Location',
                    max_length = 40, widget = forms.TextInput(
                        attrs = {
                            'class':'form-control',
                            'placeholder':'Activity Location',
                            'required': True,
                        }
                    ))

    category = forms.ChoiceField(label='Category',
                    choices = Category.CATEGORY_CHOICES,
                    widget = forms.Select(
                        attrs = {
                            'class':'form-control',
                            'required': True,
                        }
                    ))