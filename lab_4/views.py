from django.shortcuts import render

def index(request):
    return render(request, 'index.html',)

def contact(request):
    return render(request, 'contact.html',)

def project(request):
    return render(request, 'project.html',)

def register(request):
    return render(request, 'register.html',)
